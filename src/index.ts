#!/usr/bin/env node

"use strict";

import { spawnSync } from 'child_process';
import inquirer from 'inquirer';
import path from 'path';
import { recordHistorySet } from './actions/recordHistorySet';
import { recordLastCommand } from './actions/recordLastCommand';
import { recordNote } from './actions/recordNote';
import { addSearchCommandToClipboard } from './actions/addSearchCommandToClipboard';
import { refreshRepository } from './git/refreshRepository';
import { saveRepository } from './git/saveRepository';
import type { Answers } from './prompt/Answers';
import { promptOptions, PromptResult } from './prompt/promptOptions';
import { WhatChoices } from './prompt/WhatChoices';
import type { Configuration } from './setup/configuration';
import setup from './setup/setup';

inquirer.registerPrompt('autocomplete', require('inquirer-autocomplete-prompt'));
inquirer.registerPrompt('fuzzypath', require('inquirer-fuzzy-path'))

console.log('===== NEMO');
setup(__dirname)
    .then(refreshRepository)
    .then((configuration: Configuration) => {
        return promptOptions(configuration);
    }).then((promptResult: PromptResult) => {
        const configuration: Configuration = promptResult.configuration;
        const answers: Answers = promptResult.answers;
        const finalFilePath: string = path.join(configuration.getKnowledgePath(), `${answers.filename}.md`);
        if (answers.what === WhatChoices.LAST_CMD) {
            recordLastCommand(answers, configuration);
        } else if (answers.what === WhatChoices.HISTORY_SELECT) {
            recordHistorySet(answers, configuration);
        } else if (answers.what === WhatChoices.NOTE) {
            recordNote(answers, configuration);
        } else if (answers.what === WhatChoices.OPEN) {
            console.log(`ouverture de ${finalFilePath}`);
        } else if (answers.what === WhatChoices.SEARCH) {
            addSearchCommandToClipboard(answers);
            return { file: undefined, configuration };
        }
        spawnSync("nano", [finalFilePath], {
            stdio: 'inherit',
        });

        return { file: finalFilePath, configuration };
    }).then(saveRepository);


