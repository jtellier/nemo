"use strict";

import { readFileSync, writeFileSync } from 'fs';
import path from 'path';
import { Answers } from "../prompt/Answers";
import { Configuration } from '../setup/configuration';

export function recordNote(answers: Answers, configuration: Configuration): void {
    if (answers.isNewFile) {
        const noteTemplatePath: string = path.join(configuration.getTemplatePath(), 'note.md');
        let template: string = readFileSync(noteTemplatePath).toString();

        template = template.replace(/<title>/g, answers.title);
        
        const outputFilePath: string = path.join(configuration.getKnowledgePath(), `${answers.filename}.md`);
        writeFileSync(outputFilePath, template);
    } else {
        const noteFragmentTemplatePath: string = path.join(configuration.getTemplatePath(), 'note_fragment.md');
        let template: string = readFileSync(noteFragmentTemplatePath).toString();

        template = template.replace(/<subtitle>/g, answers.subtitle);

        const originalFilePath: string = path.join(configuration.getKnowledgePath(), `${answers.filename}.md`);
        const existing: string = readFileSync(originalFilePath).toString();
        const modified: string = existing + template;
        writeFileSync(originalFilePath, modified);
    }
}
