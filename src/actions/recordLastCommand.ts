"use strict";

import { readFileSync, writeFileSync } from 'fs';
import { Answers } from "../prompt/Answers";
import { Configuration } from '../setup/configuration';
import path from 'path';
import { execSync } from 'child_process';
import { homedir } from 'os';

function getHistoryCommand(): string {
    const shell: string = process.env.SHELL ?? '/bin/sh';
    if (shell.endsWith('/bash')) {
        return readFileSync(path.join(homedir(), '.bash_history')).toString()
            .split('\n')
            .slice(-2)[0]
            .replace(/^\s*\d+\s+/g, '');
    } else if(shell.endsWith('/sh')) {
        return readFileSync(path.join(homedir(), '.histfile')).toString()
            .split('\n')
            .slice(-2)[0];
    } else if(shell.endsWith('/zsh')) {
        return execSync(`zsh -c 'export HISTFILE=${path.join(homedir(), '.zsh_history')}; fc -R; fc -l 20'`).toString()
            .split('\n')
            .slice(-3)[0]
            .replace(/^\s*\d+\s+/g, '');
    } else {
        return '';
    }
}

export function getLastCommand(): string {
    return getHistoryCommand();
}

export function recordLastCommand(answers: Answers, configuration: Configuration): void {
    const lastCommand: string = getLastCommand();
    console.log('Enregistrement de :', lastCommand, '\n');

    if (answers.isNewFile) {
        const commandTemplatePath: string = path.join(configuration.getTemplatePath(), 'command.md');
        let template: string = readFileSync(commandTemplatePath).toString();

        template = template.replace(/<title>/g, answers.title);
        template = template.replace(/<subtitle>/g, answers.subtitle);
        template = template.replace(/<command>/g, lastCommand);

        const outputFilePath: string = path.join(configuration.getKnowledgePath(), `${answers.filename}.md`);
        writeFileSync(outputFilePath, template);
    } else {
        const commandFragmentTemplatePath: string = path.join(configuration.getTemplatePath(), 'command_fragment.md');
        let template: string = readFileSync(commandFragmentTemplatePath).toString();
        
        template = template.replace(/<subtitle>/g, answers.subtitle);
        template = template.replace(/<command>/g, lastCommand);

        const originalFilePath: string = path.join(configuration.getKnowledgePath(), `${answers.filename}.md`);
        const existing: string = readFileSync(originalFilePath).toString();
        const modified: string = existing + template;
        writeFileSync(originalFilePath, modified);
    }
}
