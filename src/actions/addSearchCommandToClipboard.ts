"use strict";

import clipboardy from 'clipboardy';
import { Answers } from '../prompt/Answers';

export function addSearchCommandToClipboard(answers: Answers) {
    clipboardy.writeSync(answers.searchCmd);
    console.log(`copied to clipboard: ${answers.searchCmd}`);
}
