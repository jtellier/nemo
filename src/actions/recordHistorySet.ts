"use strict";

import { readFileSync, writeFileSync } from 'fs';
import { execSync, spawnSync } from 'child_process';
import { Answers } from "../prompt/Answers";
import { Configuration } from '../setup/configuration';
import path from 'path';
import { homedir } from 'os';

function getLastHistoryRange(): string[] {
    const shell: string = process.env.SHELL ?? '/bin/sh';
    if (shell.endsWith('/bash')) {
        return readFileSync(path.join(homedir(), '.bash_history')).toString()
            .split('\n')
            .slice(-30)
            .map(command => command.replace(/^\s*\d+\s+/g, ''));
    } else if(shell.endsWith('/sh')) {
        return readFileSync(path.join(homedir(), '.histfile')).toString()
            .split('\n')
            .slice(-30)
            .map(command => command.replace(/^\s*\d+\s+/g, ''));
    } else if(shell.endsWith('/zsh')) {
        return execSync(`zsh -c 'export HISTFILE=${path.join(homedir(), '.zsh_history')}; fc -R; fc -l 20'`).toString()
            .split('\n')
            .slice(-30)
            .map(command => command.replace(/^\s*\d+\s+/g, ''));
    } else {
        return [];
    }
}

export function recordHistorySet(answers: Answers, configuration: Configuration): void {
    const lastCommands: string[] = getLastHistoryRange();
    const instructions: string[] = [
        '## Keep the commands you need.',
        '## Commented lines like these won\'t be saved.',
    ];
    writeFileSync('/tmp/history-set.to-delete', instructions.concat(lastCommands).join('\n'));
    spawnSync("nano", ['/tmp/history-set.to-delete'], {
        stdio: 'inherit',
    });
    const selectedCommands: string = readFileSync('/tmp/history-set.to-delete').toString()
        .split('\n')
        .filter(command => !command.trim().startsWith('##'))
        .join('\n');

    if (answers.isNewFile) {
        const commandTemplatePath: string = path.join(configuration.getTemplatePath(), 'command.md');
        let template: string = readFileSync(commandTemplatePath).toString();
        
        template = template.replace(/<title>/g, answers.title);
        template = template.replace(/<subtitle>/g, answers.subtitle);
        template = template.replace(/<command>/g, selectedCommands);

        const outputFilePath: string = path.join(configuration.getKnowledgePath(), `${answers.filename}.md`);
        writeFileSync(outputFilePath, template);
    } else {
        const commandFragmentTemplatePath: string = path.join(configuration.getTemplatePath(), 'command_fragment.md');
        let template: string = readFileSync(commandFragmentTemplatePath).toString();
        
        template = template.replace(/<subtitle>/g, answers.subtitle);
        template = template.replace(/<command>/g, selectedCommands);
        
        const originalFilePath: string = path.join(configuration.getKnowledgePath(), `${answers.filename}.md`);
        const existing: string = readFileSync(originalFilePath).toString();
        const modified: string = existing + template;
        writeFileSync(originalFilePath, modified);
    }
}
