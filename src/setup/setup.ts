"use strict";

import { existsSync } from "fs";
import { mkdir, readFile, writeFile } from "fs/promises";
import inquirer from "inquirer";
import os from 'os';
import path from 'path';
import { Configuration } from "./configuration";

function querySetupOptions() {
    return inquirer.prompt([
        {
            type: 'fuzzypath',
            name: 'path',
            message: 'Où sauvegarder les fichiers ?',
            rootPath: os.homedir(),
            itemType: 'directory',
            depthLimit: 1,
            suggestOnly: true,
        }
    ]);
}

export default async function (currentScriptPath: string): Promise<any> {
    const homedir: string = os.homedir();
    const configurationFilePath: string = path.join(homedir, '.nemo.config.json');
    if (!existsSync(configurationFilePath)) {
        console.log("C'est votre première utilisation sur cette machine:");
        const answers: any = await querySetupOptions();
        const configuration: Configuration = new Configuration({
            knowledgePath: answers.path,
            shell: process.env.SHELL,
            gitRemote: 'origin',
            gitBranch: 'master',
        });
        await mkdir(configuration.getKnowledgePath(), { recursive: true });
        await mkdir(configuration.getTemplatePath(), { recursive: true });
        await writeFile(configurationFilePath, configuration.toPrintableJson());
        await Promise
            .all(['command.md', 'command_fragment.md', 'note.md', 'note_fragment.md']
            .map(async templateFileName => {
                path.join(currentScriptPath, '..', 'templates', templateFileName)
                return readFile(path.join(currentScriptPath, '..', 'templates', templateFileName))
                    .then(template => template.toString())
                    .then(templateContent => writeFile(path.join(configuration.getTemplatePath(), templateFileName), templateContent)
                );
            }));
        const configurationFile = await readFile(configurationFilePath);
        const configurationFileJson: any = JSON.parse(configurationFile.toString());
        return new Configuration(configurationFileJson);
    } else {
        return readFile(configurationFilePath)
            .then(configurationFile => configurationFile.toString())
            .then(configurationFileContent => JSON.parse(configurationFileContent))
            .then(configurationFileJson => ({ ...configurationFileJson, shell: process.env.SHELL }))
            .then(configurationFileJson => new Configuration(configurationFileJson));
    }
}