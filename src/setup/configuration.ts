"use strict";

import path from "path";

export class Configuration {
    private knowledgePath: string = '/tmp/nemo-tmp';
    private templatePath: string = '/tmp/nemo-tmp/templates';
    private gitRemote: string = 'origin';
    private gitBranch: string = 'master';

    constructor(jsonConfiguration: any) {
        this.knowledgePath = jsonConfiguration.knowledgePath;
        this.templatePath = path.join(jsonConfiguration.knowledgePath, 'templates');
        this.gitRemote = jsonConfiguration.gitRemote;
        this.gitBranch = jsonConfiguration.gitBranch;
    }

    getKnowledgePath(): string {
        return this.knowledgePath;
    }
    
    getTemplatePath(): string {
        return this.templatePath;
    }

    getGitRemote(): string {
        return this.gitRemote;
    }

    getGitBranch(): string {
        return this.gitBranch;
    }

    toPrintableJson(): string {
        return JSON.stringify({
            knowledgePath: this.knowledgePath,
            gitRemote: this.gitRemote,
            gitBranch: this.gitBranch,
        }, null, 2);
    }
}
