"use strict";

import { Configuration } from '../setup/configuration';
import path from 'path';
import simpleGit, { SimpleGit } from 'simple-git';
import { existsSync } from 'fs';
import { homedir } from 'os';

export async function refreshRepository(configuration: Configuration): Promise<Configuration> {
    const git: SimpleGit = simpleGit({
        baseDir: configuration.getKnowledgePath(),
        binary: 'git',
    });
    if (!existsSync(path.join(configuration.getKnowledgePath(), '.git'))) {
        console.log(`${configuration.getKnowledgePath()} is not a git repository, initializing ...`);
        await git.init()
            .add('./*')
            .commit('initial commit');
    }
    const origin = (await git.getRemotes())
        .map(remote => remote.name)
        .filter(remoteName => remoteName === configuration.getGitRemote())[0];
    if (!origin) {
        console.log(`${configuration.getKnowledgePath()} has no remote named origin. You can the remote name this parameter in ${path.join(homedir(), '.nemo.config.json')}`);
        return configuration;
    } else {
        console.log('pulling remote changes ...');
        git.fetch()
            .pull(configuration.getGitRemote(), configuration.getGitBranch())
            .catch(error => {
                console.error(`Could not pull branch '${configuration.getGitBranch()}' from origin '${configuration.getGitRemote()}' : ${error}`);
                return configuration;
            });
        return configuration;
    }
}
