"use strict";

import { spawn } from 'child_process';
import path from 'path';
import simpleGit, { SimpleGit } from 'simple-git';
import { openSync } from 'fs';
import { homedir } from 'os';

export async function saveRepository(ret: any): Promise<any> {
    if (ret.file) {
        const git: SimpleGit = simpleGit({
            baseDir: ret.configuration.getKnowledgePath(),
            binary: 'git',
        });
        await git
            .add(ret.file)
            .commit(`update ${ret.file}`);
        const origin = (await git.getRemotes())
            .map(remote => remote.name)
            .filter(remoteName => remoteName === ret.configuration.getGitRemote())[0];
        if (origin) {
            console.log('saving changes ...');
            const logFile = path.join(homedir(), '.nemo.log');
            var out = openSync(logFile, 'a');
            var err = openSync(logFile, 'a');
            const child = spawn(
                `pushd ${ret.configuration.getKnowledgePath()} && git push -u ${ret.configuration.getGitRemote()} ${ret.configuration.getGitBranch()}`,
                { detached: true, stdio: ['ignore', out, err], shell: process.env.SHELL }
            );
            child.unref();
        }
    }
}
