"use strict";

import { WhatChoices } from "./WhatChoices";


export class Answers {
    what: WhatChoices;
    searchCmd: string;
    isNewFile: boolean;
    filename: string;
    title: string;
    subtitle: string;

    constructor(
        what: WhatChoices,
        searchCmd: string,
        isNewFile: boolean,
        filename: string,
        title: string,
        subtitle: string
    ) {
        this.what = WhatChoices[what];
        this.searchCmd = searchCmd;
        this.isNewFile = isNewFile;
        this.filename = filename;
        this.title = title;
        this.subtitle = subtitle;
    }
}
