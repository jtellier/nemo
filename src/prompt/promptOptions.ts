"use strict";

import { Dirent, existsSync, readdir, readFileSync } from 'fs';
import inquirer from 'inquirer';
import { WhatChoices } from "./WhatChoices";
import { Answers } from "./Answers";
import { Configuration } from '../setup/configuration';
import path from 'path';
import { getLastCommand } from '../actions/recordLastCommand';

export class PromptResult {
    public answers: Answers;
    public configuration: Configuration;

    constructor(answers: Answers, configuration: Configuration) {
        this.answers = answers;
        this.configuration = configuration;
    }
}

export async function promptOptions(configuration: Configuration): Promise<PromptResult> {
    const whatAnswer: inquirer.Answers = await inquirer.prompt([{
        type: 'list',
        name: 'what',
        message: 'Que faire',
        choices: [
            {
                name: 'sauvegarder dernière commande',
                value: WhatChoices.LAST_CMD,
            },
            {
                name: 'sauvegarder historique de commandes',
                value: WhatChoices.HISTORY_SELECT,
            },
            {
                name: 'sauvegarder note',
                value: WhatChoices.NOTE,
            },
            {
                name: 'ouvrir fichier',
                value: WhatChoices.OPEN,
            },
            {
                name: 'chercher',
                value: WhatChoices.SEARCH,
            },
        ],
    }]);
    if (WhatChoices[<WhatChoices>whatAnswer.what] === WhatChoices.LAST_CMD) {
        console.log('=> ' + getLastCommand());
    }
    const searchAnswer: inquirer.Answers = await inquirer.prompt([{
        type: 'autocomplete',
        name: 'search_cmd',
        message: 'Quelle commande',
        source: function (answersSoFar: any, input: string): Promise<string[]> {
            return new Promise((resolve, reject) => {
                readdir(configuration.getKnowledgePath(), { withFileTypes: true }, (error: NodeJS.ErrnoException | null, files: Dirent[]) => {
                    if (error) {
                        reject(error);
                    } else {
                        if (!input || input.length < 2) {
                            return [];
                        }
                        const matches: string[] = files
                            .filter((file: Dirent): boolean => file.isFile())
                            .map((file: Dirent): string => file.name)
                            .map((file: string): string => {
                                const filePath = path.join(configuration.getKnowledgePath(), file);
                                return readFileSync(filePath).toString();
                            })
                            .filter((content: string): boolean => {
                                return content.indexOf(input) > -1;
                            })
                            .join('\n')
                            .split('\n')
                            .filter((line: string) => {
                                return line.indexOf(input) > -1;
                            })
                            .sort(function (a: string, b: string): number {
                                return a.toLowerCase().localeCompare(b.toLowerCase());
                            })
                            .filter((fileName:string, currentIndex: number, array: string[]): boolean => array.indexOf(fileName) === currentIndex);
                        resolve(matches);
                    }
                });
            });
        },
        when: (): boolean => {
            return WhatChoices[<WhatChoices>whatAnswer.what] === WhatChoices.SEARCH;
        }
    }]);
    const filenameAnswer: inquirer.Answers = await inquirer.prompt([{
        type: 'autocomplete',
        name: 'filename',
        message: 'Où le mettre',
        suggestOnly: true,
        validate: function (input: string): boolean {
            return input.length > 0;
        },
        source: function (answersSoFar: any, input: string): Promise<string[]> {
            return new Promise((resolve, reject) => {
                readdir(configuration.getKnowledgePath(), { withFileTypes: true }, (error: NodeJS.ErrnoException | null, files: Dirent[]) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(files
                            .filter((file: Dirent): boolean => file.isFile())
                            .map((file: Dirent): string => file.name)
                            .map((file: string): string => file.replace('.md', ''))
                            .filter((file: string): string[] => file.match(input) ?? []));
                    }
                });
            });
        },
        when: (): boolean => {
            return WhatChoices[<WhatChoices>whatAnswer.what] !== WhatChoices.SEARCH;
        }
    }]);
    const targetFilePath = path.join(configuration.getKnowledgePath(), `${filenameAnswer.filename}.md`);
    const isNewFile = !existsSync(targetFilePath);
    const titleAnswer: inquirer.Answers = await inquirer.prompt([{
        type: 'input',
        name: 'title',
        message: 'Titre de fichier',
        when: (): boolean => {
            return isNewFile
                && WhatChoices[<WhatChoices>whatAnswer.what] !== WhatChoices.OPEN
                && WhatChoices[<WhatChoices>whatAnswer.what] !== WhatChoices.SEARCH;
        }
    }]);
    const subtitleAnswer: inquirer.Answers = await inquirer.prompt([{
        type: 'input',
        name: 'subtitle',
        message: 'Titre de section',
        when: (): boolean => {
            return !(WhatChoices[<WhatChoices>whatAnswer.what] === WhatChoices.NOTE && isNewFile)
                && WhatChoices[<WhatChoices>whatAnswer.what] !== WhatChoices.OPEN
                && WhatChoices[<WhatChoices>whatAnswer.what] !== WhatChoices.SEARCH;
        }
    }]);
    return new PromptResult(new Answers(
            whatAnswer.what,
            searchAnswer.search_cmd,
            isNewFile,
            filenameAnswer.filename,
            titleAnswer.title,
            subtitleAnswer.subtitle,
        ), configuration);
    ;
}
