"use strict";

export enum WhatChoices {
    LAST_CMD = 'LAST_CMD',
    HISTORY_SELECT = 'HISTORY_SELECT',
    NOTE = 'NOTE',
    OPEN = 'OPEN',
    SEARCH = 'SEARCH'
}
