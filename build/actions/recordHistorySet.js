"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.recordHistorySet = void 0;
const fs_1 = require("fs");
const child_process_1 = require("child_process");
const path_1 = __importDefault(require("path"));
const os_1 = require("os");
function getLastHistoryRange() {
    const shell = process.env.SHELL ?? '/bin/sh';
    if (shell.endsWith('/bash')) {
        return fs_1.readFileSync(path_1.default.join(os_1.homedir(), '.bash_history')).toString()
            .split('\n')
            .slice(-30)
            .map(command => command.replace(/^\s*\d+\s+/g, ''));
    }
    else if (shell.endsWith('/sh')) {
        return fs_1.readFileSync(path_1.default.join(os_1.homedir(), '.histfile')).toString()
            .split('\n')
            .slice(-30)
            .map(command => command.replace(/^\s*\d+\s+/g, ''));
    }
    else if (shell.endsWith('/zsh')) {
        return child_process_1.execSync(`zsh -c 'export HISTFILE=${path_1.default.join(os_1.homedir(), '.zsh_history')}; fc -R; fc -l 20'`).toString()
            .split('\n')
            .slice(-30)
            .map(command => command.replace(/^\s*\d+\s+/g, ''));
    }
    else {
        return [];
    }
}
function recordHistorySet(answers, configuration) {
    const lastCommands = getLastHistoryRange();
    const instructions = [
        '## Keep the commands you need.',
        '## Commented lines like these won\'t be saved.',
    ];
    fs_1.writeFileSync('/tmp/history-set.to-delete', instructions.concat(lastCommands).join('\n'));
    child_process_1.spawnSync("nano", ['/tmp/history-set.to-delete'], {
        stdio: 'inherit',
    });
    const selectedCommands = fs_1.readFileSync('/tmp/history-set.to-delete').toString()
        .split('\n')
        .filter(command => !command.trim().startsWith('##'))
        .join('\n');
    if (answers.isNewFile) {
        const commandTemplatePath = path_1.default.join(configuration.getTemplatePath(), 'command.md');
        let template = fs_1.readFileSync(commandTemplatePath).toString();
        template = template.replace(/<title>/g, answers.title);
        template = template.replace(/<subtitle>/g, answers.subtitle);
        template = template.replace(/<command>/g, selectedCommands);
        const outputFilePath = path_1.default.join(configuration.getKnowledgePath(), `${answers.filename}.md`);
        fs_1.writeFileSync(outputFilePath, template);
    }
    else {
        const commandFragmentTemplatePath = path_1.default.join(configuration.getTemplatePath(), 'command_fragment.md');
        let template = fs_1.readFileSync(commandFragmentTemplatePath).toString();
        template = template.replace(/<subtitle>/g, answers.subtitle);
        template = template.replace(/<command>/g, selectedCommands);
        const originalFilePath = path_1.default.join(configuration.getKnowledgePath(), `${answers.filename}.md`);
        const existing = fs_1.readFileSync(originalFilePath).toString();
        const modified = existing + template;
        fs_1.writeFileSync(originalFilePath, modified);
    }
}
exports.recordHistorySet = recordHistorySet;
