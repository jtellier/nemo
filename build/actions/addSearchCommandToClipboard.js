"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.addSearchCommandToClipboard = void 0;
const clipboardy_1 = __importDefault(require("clipboardy"));
function addSearchCommandToClipboard(answers) {
    clipboardy_1.default.writeSync(answers.searchCmd);
    console.log(`copied to clipboard: ${answers.searchCmd}`);
}
exports.addSearchCommandToClipboard = addSearchCommandToClipboard;
