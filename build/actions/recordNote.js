"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.recordNote = void 0;
const fs_1 = require("fs");
const path_1 = __importDefault(require("path"));
function recordNote(answers, configuration) {
    if (answers.isNewFile) {
        const noteTemplatePath = path_1.default.join(configuration.getTemplatePath(), 'note.md');
        let template = fs_1.readFileSync(noteTemplatePath).toString();
        template = template.replace(/<title>/g, answers.title);
        const outputFilePath = path_1.default.join(configuration.getKnowledgePath(), `${answers.filename}.md`);
        fs_1.writeFileSync(outputFilePath, template);
    }
    else {
        const noteFragmentTemplatePath = path_1.default.join(configuration.getTemplatePath(), 'note_fragment.md');
        let template = fs_1.readFileSync(noteFragmentTemplatePath).toString();
        template = template.replace(/<subtitle>/g, answers.subtitle);
        const originalFilePath = path_1.default.join(configuration.getKnowledgePath(), `${answers.filename}.md`);
        const existing = fs_1.readFileSync(originalFilePath).toString();
        const modified = existing + template;
        fs_1.writeFileSync(originalFilePath, modified);
    }
}
exports.recordNote = recordNote;
