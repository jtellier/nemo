"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const promises_1 = require("fs/promises");
const inquirer_1 = __importDefault(require("inquirer"));
const os_1 = __importDefault(require("os"));
const path_1 = __importDefault(require("path"));
const configuration_1 = require("./configuration");
function querySetupOptions() {
    return inquirer_1.default.prompt([
        {
            type: 'fuzzypath',
            name: 'path',
            message: 'Où sauvegarder les fichiers ?',
            rootPath: os_1.default.homedir(),
            itemType: 'directory',
            depthLimit: 1,
            suggestOnly: true,
        }
    ]);
}
async function default_1(currentScriptPath) {
    const homedir = os_1.default.homedir();
    const configurationFilePath = path_1.default.join(homedir, '.nemo.config.json');
    if (!fs_1.existsSync(configurationFilePath)) {
        console.log("C'est votre première utilisation sur cette machine:");
        const answers = await querySetupOptions();
        const configuration = new configuration_1.Configuration({
            knowledgePath: answers.path,
            shell: process.env.SHELL,
            gitRemote: 'origin',
            gitBranch: 'master',
        });
        await promises_1.mkdir(configuration.getKnowledgePath(), { recursive: true });
        await promises_1.mkdir(configuration.getTemplatePath(), { recursive: true });
        await promises_1.writeFile(configurationFilePath, configuration.toPrintableJson());
        await Promise
            .all(['command.md', 'command_fragment.md', 'note.md', 'note_fragment.md']
            .map(async (templateFileName) => {
            path_1.default.join(currentScriptPath, '..', 'templates', templateFileName);
            return promises_1.readFile(path_1.default.join(currentScriptPath, '..', 'templates', templateFileName))
                .then(template => template.toString())
                .then(templateContent => promises_1.writeFile(path_1.default.join(configuration.getTemplatePath(), templateFileName), templateContent));
        }));
        const configurationFile = await promises_1.readFile(configurationFilePath);
        const configurationFileJson = JSON.parse(configurationFile.toString());
        return new configuration_1.Configuration(configurationFileJson);
    }
    else {
        return promises_1.readFile(configurationFilePath)
            .then(configurationFile => configurationFile.toString())
            .then(configurationFileContent => JSON.parse(configurationFileContent))
            .then(configurationFileJson => ({ ...configurationFileJson, shell: process.env.SHELL }))
            .then(configurationFileJson => new configuration_1.Configuration(configurationFileJson));
    }
}
exports.default = default_1;
