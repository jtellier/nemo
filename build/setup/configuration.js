"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Configuration = void 0;
const path_1 = __importDefault(require("path"));
class Configuration {
    constructor(jsonConfiguration) {
        this.knowledgePath = '/tmp/nemo-tmp';
        this.templatePath = '/tmp/nemo-tmp/templates';
        this.gitRemote = 'origin';
        this.gitBranch = 'master';
        this.knowledgePath = jsonConfiguration.knowledgePath;
        this.templatePath = path_1.default.join(jsonConfiguration.knowledgePath, 'templates');
        this.gitRemote = jsonConfiguration.gitRemote;
        this.gitBranch = jsonConfiguration.gitBranch;
    }
    getKnowledgePath() {
        return this.knowledgePath;
    }
    getTemplatePath() {
        return this.templatePath;
    }
    getGitRemote() {
        return this.gitRemote;
    }
    getGitBranch() {
        return this.gitBranch;
    }
    toPrintableJson() {
        return JSON.stringify({
            knowledgePath: this.knowledgePath,
            gitRemote: this.gitRemote,
            gitBranch: this.gitBranch,
        }, null, 2);
    }
}
exports.Configuration = Configuration;
