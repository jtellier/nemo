"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.promptOptions = exports.PromptResult = void 0;
const fs_1 = require("fs");
const inquirer_1 = __importDefault(require("inquirer"));
const WhatChoices_1 = require("./WhatChoices");
const Answers_1 = require("./Answers");
const path_1 = __importDefault(require("path"));
const recordLastCommand_1 = require("../actions/recordLastCommand");
class PromptResult {
    constructor(answers, configuration) {
        this.answers = answers;
        this.configuration = configuration;
    }
}
exports.PromptResult = PromptResult;
async function promptOptions(configuration) {
    const whatAnswer = await inquirer_1.default.prompt([{
            type: 'list',
            name: 'what',
            message: 'Que faire',
            choices: [
                {
                    name: 'sauvegarder dernière commande',
                    value: WhatChoices_1.WhatChoices.LAST_CMD,
                },
                {
                    name: 'sauvegarder historique de commandes',
                    value: WhatChoices_1.WhatChoices.HISTORY_SELECT,
                },
                {
                    name: 'sauvegarder note',
                    value: WhatChoices_1.WhatChoices.NOTE,
                },
                {
                    name: 'ouvrir fichier',
                    value: WhatChoices_1.WhatChoices.OPEN,
                },
                {
                    name: 'chercher',
                    value: WhatChoices_1.WhatChoices.SEARCH,
                },
            ],
        }]);
    if (WhatChoices_1.WhatChoices[whatAnswer.what] === WhatChoices_1.WhatChoices.LAST_CMD) {
        console.log('=> ' + recordLastCommand_1.getLastCommand());
    }
    const searchAnswer = await inquirer_1.default.prompt([{
            type: 'autocomplete',
            name: 'search_cmd',
            message: 'Quelle commande',
            source: function (answersSoFar, input) {
                return new Promise((resolve, reject) => {
                    fs_1.readdir(configuration.getKnowledgePath(), { withFileTypes: true }, (error, files) => {
                        if (error) {
                            reject(error);
                        }
                        else {
                            if (!input || input.length < 2) {
                                return [];
                            }
                            const matches = files
                                .filter((file) => file.isFile())
                                .map((file) => file.name)
                                .map((file) => {
                                const filePath = path_1.default.join(configuration.getKnowledgePath(), file);
                                return fs_1.readFileSync(filePath).toString();
                            })
                                .filter((content) => {
                                return content.indexOf(input) > -1;
                            })
                                .join('\n')
                                .split('\n')
                                .filter((line) => {
                                return line.indexOf(input) > -1;
                            })
                                .sort(function (a, b) {
                                return a.toLowerCase().localeCompare(b.toLowerCase());
                            })
                                .filter((fileName, currentIndex, array) => array.indexOf(fileName) === currentIndex);
                            resolve(matches);
                        }
                    });
                });
            },
            when: () => {
                return WhatChoices_1.WhatChoices[whatAnswer.what] === WhatChoices_1.WhatChoices.SEARCH;
            }
        }]);
    const filenameAnswer = await inquirer_1.default.prompt([{
            type: 'autocomplete',
            name: 'filename',
            message: 'Où le mettre',
            suggestOnly: true,
            validate: function (input) {
                return input.length > 0;
            },
            source: function (answersSoFar, input) {
                return new Promise((resolve, reject) => {
                    fs_1.readdir(configuration.getKnowledgePath(), { withFileTypes: true }, (error, files) => {
                        if (error) {
                            reject(error);
                        }
                        else {
                            resolve(files
                                .filter((file) => file.isFile())
                                .map((file) => file.name)
                                .map((file) => file.replace('.md', ''))
                                .filter((file) => file.match(input) ?? []));
                        }
                    });
                });
            },
            when: () => {
                return WhatChoices_1.WhatChoices[whatAnswer.what] !== WhatChoices_1.WhatChoices.SEARCH;
            }
        }]);
    const targetFilePath = path_1.default.join(configuration.getKnowledgePath(), `${filenameAnswer.filename}.md`);
    const isNewFile = !fs_1.existsSync(targetFilePath);
    const titleAnswer = await inquirer_1.default.prompt([{
            type: 'input',
            name: 'title',
            message: 'Titre de fichier',
            when: () => {
                return isNewFile
                    && WhatChoices_1.WhatChoices[whatAnswer.what] !== WhatChoices_1.WhatChoices.OPEN
                    && WhatChoices_1.WhatChoices[whatAnswer.what] !== WhatChoices_1.WhatChoices.SEARCH;
            }
        }]);
    const subtitleAnswer = await inquirer_1.default.prompt([{
            type: 'input',
            name: 'subtitle',
            message: 'Titre de section',
            when: () => {
                return !(WhatChoices_1.WhatChoices[whatAnswer.what] === WhatChoices_1.WhatChoices.NOTE && isNewFile)
                    && WhatChoices_1.WhatChoices[whatAnswer.what] !== WhatChoices_1.WhatChoices.OPEN
                    && WhatChoices_1.WhatChoices[whatAnswer.what] !== WhatChoices_1.WhatChoices.SEARCH;
            }
        }]);
    return new PromptResult(new Answers_1.Answers(whatAnswer.what, searchAnswer.search_cmd, isNewFile, filenameAnswer.filename, titleAnswer.title, subtitleAnswer.subtitle), configuration);
    ;
}
exports.promptOptions = promptOptions;
