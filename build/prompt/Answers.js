"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Answers = void 0;
const WhatChoices_1 = require("./WhatChoices");
class Answers {
    constructor(what, searchCmd, isNewFile, filename, title, subtitle) {
        this.what = WhatChoices_1.WhatChoices[what];
        this.searchCmd = searchCmd;
        this.isNewFile = isNewFile;
        this.filename = filename;
        this.title = title;
        this.subtitle = subtitle;
    }
}
exports.Answers = Answers;
