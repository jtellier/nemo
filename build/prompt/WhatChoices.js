"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WhatChoices = void 0;
var WhatChoices;
(function (WhatChoices) {
    WhatChoices["LAST_CMD"] = "LAST_CMD";
    WhatChoices["HISTORY_SELECT"] = "HISTORY_SELECT";
    WhatChoices["NOTE"] = "NOTE";
    WhatChoices["OPEN"] = "OPEN";
    WhatChoices["SEARCH"] = "SEARCH";
})(WhatChoices = exports.WhatChoices || (exports.WhatChoices = {}));
