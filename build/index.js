#!/usr/bin/env node
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const child_process_1 = require("child_process");
const inquirer_1 = __importDefault(require("inquirer"));
const path_1 = __importDefault(require("path"));
const recordHistorySet_1 = require("./actions/recordHistorySet");
const recordLastCommand_1 = require("./actions/recordLastCommand");
const recordNote_1 = require("./actions/recordNote");
const addSearchCommandToClipboard_1 = require("./actions/addSearchCommandToClipboard");
const refreshRepository_1 = require("./git/refreshRepository");
const saveRepository_1 = require("./git/saveRepository");
const promptOptions_1 = require("./prompt/promptOptions");
const WhatChoices_1 = require("./prompt/WhatChoices");
const setup_1 = __importDefault(require("./setup/setup"));
inquirer_1.default.registerPrompt('autocomplete', require('inquirer-autocomplete-prompt'));
inquirer_1.default.registerPrompt('fuzzypath', require('inquirer-fuzzy-path'));
console.log('===== NEMO');
setup_1.default(__dirname)
    .then(refreshRepository_1.refreshRepository)
    .then((configuration) => {
    return promptOptions_1.promptOptions(configuration);
}).then((promptResult) => {
    const configuration = promptResult.configuration;
    const answers = promptResult.answers;
    const finalFilePath = path_1.default.join(configuration.getKnowledgePath(), `${answers.filename}.md`);
    if (answers.what === WhatChoices_1.WhatChoices.LAST_CMD) {
        recordLastCommand_1.recordLastCommand(answers, configuration);
    }
    else if (answers.what === WhatChoices_1.WhatChoices.HISTORY_SELECT) {
        recordHistorySet_1.recordHistorySet(answers, configuration);
    }
    else if (answers.what === WhatChoices_1.WhatChoices.NOTE) {
        recordNote_1.recordNote(answers, configuration);
    }
    else if (answers.what === WhatChoices_1.WhatChoices.OPEN) {
        console.log(`ouverture de ${finalFilePath}`);
    }
    else if (answers.what === WhatChoices_1.WhatChoices.SEARCH) {
        addSearchCommandToClipboard_1.addSearchCommandToClipboard(answers);
        return { file: undefined, configuration };
    }
    child_process_1.spawnSync("nano", [finalFilePath], {
        stdio: 'inherit',
    });
    return { file: finalFilePath, configuration };
}).then(saveRepository_1.saveRepository);
