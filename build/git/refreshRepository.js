"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.refreshRepository = void 0;
const path_1 = __importDefault(require("path"));
const simple_git_1 = __importDefault(require("simple-git"));
const fs_1 = require("fs");
const os_1 = require("os");
async function refreshRepository(configuration) {
    const git = simple_git_1.default({
        baseDir: configuration.getKnowledgePath(),
        binary: 'git',
    });
    if (!fs_1.existsSync(path_1.default.join(configuration.getKnowledgePath(), '.git'))) {
        console.log(`${configuration.getKnowledgePath()} is not a git repository, initializing ...`);
        await git.init()
            .add('./*')
            .commit('initial commit');
    }
    const origin = (await git.getRemotes())
        .map(remote => remote.name)
        .filter(remoteName => remoteName === configuration.getGitRemote())[0];
    if (!origin) {
        console.log(`${configuration.getKnowledgePath()} has no remote named origin. You can the remote name this parameter in ${path_1.default.join(os_1.homedir(), '.nemo.config.json')}`);
        return configuration;
    }
    else {
        console.log('pulling remote changes ...');
        git.fetch()
            .pull(configuration.getGitRemote(), configuration.getGitBranch())
            .catch(error => {
            console.error(`Could not pull branch '${configuration.getGitBranch()}' from origin '${configuration.getGitRemote()}' : ${error}`);
            return configuration;
        });
        return configuration;
    }
}
exports.refreshRepository = refreshRepository;
