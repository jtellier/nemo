"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.saveRepository = void 0;
const child_process_1 = require("child_process");
const path_1 = __importDefault(require("path"));
const simple_git_1 = __importDefault(require("simple-git"));
const fs_1 = require("fs");
const os_1 = require("os");
async function saveRepository(ret) {
    if (ret.file) {
        const git = simple_git_1.default({
            baseDir: ret.configuration.getKnowledgePath(),
            binary: 'git',
        });
        await git
            .add(ret.file)
            .commit(`update ${ret.file}`);
        const origin = (await git.getRemotes())
            .map(remote => remote.name)
            .filter(remoteName => remoteName === ret.configuration.getGitRemote())[0];
        if (origin) {
            console.log('saving changes ...');
            const logFile = path_1.default.join(os_1.homedir(), '.nemo.log');
            var out = fs_1.openSync(logFile, 'a');
            var err = fs_1.openSync(logFile, 'a');
            const child = child_process_1.spawn(`pushd ${ret.configuration.getKnowledgePath()} && git push -u ${ret.configuration.getGitRemote()} ${ret.configuration.getGitBranch()}`, { detached: true, stdio: ['ignore', out, err], shell: process.env.SHELL });
            child.unref();
        }
    }
}
exports.saveRepository = saveRepository;
