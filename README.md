# Nemo

Ok let's type this command. Hmm, not working, I'm missing this parameter. **Heck, what was it again ?** How come I never remember !

Nemo records on a git repository the commands, history of commands, notes you ask it to save and allow you search through theses records.

## Configuration

At the first run of the command, you'll be asked to choose a directory where you want to save the templates *and* the notes and commands.

The default remote is `origin`, and the default branch is `master` but you can change all of that in `.nemo.config.json`.

Here is the default configuration file :

```json
{
    "knowledgePath": "<some path you configured at startup>",
    "gitRemote": "origin",
    "gitBranch": "master",
}
```


## Records' templates

All templates are in the repository specified on the first launch of the command. These are in markdown with the following placeholders :

```
<title> # the title at the head of the file
<subtitle> # the title of the section you are recording
<command> # a placeholder for the command or list of commands
```

## Recommandations

### Write your history immediatly

Nemo relies on the history files, so if you need your last commands to be recorded you should configure your shell to do so :

#### Bash

https://askubuntu.com/questions/67283/is-it-possible-to-make-writing-to-bash-history-immediate

#### ZSH

https://askubuntu.com/questions/23630/how-do-you-share-history-between-terminals-in-zsh
